<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Visit;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $places = Place::all();
        return response()->json($places, 200);
    }

    public function getHome()
    {
        $places = Place::all();
        //Retorna la vista
        return view('places.index')->withPlaces($places);
    }
}
