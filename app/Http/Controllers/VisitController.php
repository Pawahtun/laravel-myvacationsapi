<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visit;
use App\Place;

class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex($place_id)
    {
        $visits = Visit::all()->where('place_id', $place_id );
        return response()->json($visits, 200);

    }

    public function getHome($place_id)
    {
        $visits = Visit::all()->where('place_id', $place_id );
        return view('visits.index')->withVisits($visits);
    }

}
