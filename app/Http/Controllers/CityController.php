<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        //Retorna la vista
        //return view('places.index')->withPlaces($places);
        return response()->json($cities, 200);
    }

}
