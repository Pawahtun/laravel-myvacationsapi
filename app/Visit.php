<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = 'visit_places';
    protected $fillable = ['id', 'place_id', 'name', 'img', 'status'];

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

}
