<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'places';
    protected $fillable = ['id', 'name', 'likes', 'img'];

    public function visits()
    {
        return $this->hasMany('App\Visit');
    }
}
