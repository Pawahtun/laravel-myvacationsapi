<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta de ejemplo
Route::get('/home', 'PlaceController@getHome'); 
Route::get('/visits/{places_id}', 'VisitController@getHome');