<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._head')
  </head>

  <body>

    
    <!-- Container-->
    <div class="container">


      @yield('content')


    </div> <!-- End of .container -->

  </body>
</html>