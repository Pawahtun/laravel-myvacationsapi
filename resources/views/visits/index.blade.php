@extends('main')

@section('title', '| All Visits')

@section('content')

    <h1>Página secuandaria de My Vacations</h1>
    <div class="row">
		<div class="col-md-8">
			<h1>Places</h1>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Image</th>
					</tr>
				</thead>

				<tbody>
					@foreach ( $visits as  $visit)
					<tr>
						<th> {{ $visit->id }} </th>
						<td> {{ $visit->name }} </td>
						<td> <img src="{{ $visit->img }}"> </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div> <!-- end of col-md-8 -->

@endsection