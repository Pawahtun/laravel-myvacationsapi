@extends('main')

@section('title', '| All Places')

@section('content')

    <h1>Estás en la página principal de My Vacations en Laravel</h1>
    <div class="row">
		<div class="col-md-8">
			<h1>Places</h1>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Likes</th>
						<th>Image</th>
					</tr>
				</thead>

				<tbody>
					@foreach ( $places as  $place)
					<tr>
						<th> {{ $place->id }} </th>
						<td> {{ $place->name }} </td>
						<td> {{ $place->likes }} </td>
						<td> <img src="{{ $place->img }}"> </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div> <!-- end of col-md-8 -->

@endsection